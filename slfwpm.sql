-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Време на генериране: 
-- Версия на сървъра: 5.5.27
-- Версия на PHP: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- БД: `slfwpm`
--

-- --------------------------------------------------------

--
-- Структура на таблица `pm`
--

CREATE TABLE IF NOT EXISTS `pm` (
  `id` bigint(20) NOT NULL,
  `id2` int(11) NOT NULL,
  `title` varchar(256) NOT NULL,
  `user1` bigint(20) NOT NULL,
  `user2` bigint(20) NOT NULL,
  `message` text NOT NULL,
  `timestamp` int(10) NOT NULL,
  `user1read` varchar(3) NOT NULL,
  `user2read` varchar(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Ссхема на данните от таблица `pm`
--

INSERT INTO `pm` (`id`, `id2`, `title`, `user1`, `user2`, `message`, `timestamp`, `user1read`, `user2read`) VALUES
(3, 1, 'asdad', 1, 2, 'asdad ;d', 1410036637, 'yes', 'yes'),
(2, 1, 'adadsad', 0, 1, 'asdad', 1409785221, 'yes', 'no'),
(1, 1, 'asd', 0, 0, 'admin', 5, '1', '1');

-- --------------------------------------------------------

--
-- Структура на таблица `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `avatar` text NOT NULL,
  `signup_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Ссхема на данните от таблица `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `avatar`, `signup_date`) VALUES
(1, 'admin', 'admin5', 'vasko_slavchev@abv.bg', 'admin', '2014-09-04 00:00:00'),
(7, 'vasko', 'vasko5', 'vasko_slavchev@abv.bg', '', '2014-09-07 00:00:00'),
(5, 'vasil', 'vasil5', 'vasko_slavchev@abv.bg', 'vasil', '2014-09-07 00:00:00'),
(3, 'vaseto', 'vaseto5', 'vasko_slavchev@abv.bg', 'vaseto', '2014-09-07 00:00:00'),
(4, 'vaseto5', 'vaseto5', 'vasko_slavchev@abv.bg', 'vaseto5', '2014-09-07 00:00:00'),
(2, 'admin5', 'admin5', 'vasko_slavchev@abv.bg', 'admin5', '2014-09-07 00:00:00'),
(8, 'vasko5', 'vasko5', 'vasko_slavchev@abv.bg', 'vasko5', '2014-09-07 00:00:00'),
(6, 'vasil5', 'vasil5', 'vasko_slavchev@abv.bg', 'vasil5', '2014-09-07 00:00:00'),
(9, 'asdasd', 'asdasd', 'asdasd@abv.bg', 'asdasd', '2014-09-07 04:48:00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
