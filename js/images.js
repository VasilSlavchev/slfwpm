/* -------------------- This is images.js ----------------------
 * SLFWPM - Simple Login Form With PM Messages by Vaseto.net
 * This is config file for Simple PM Message by Vaseto^^ 2014.
 * More at: (VasilSlavchev.info or vasilslavchev@gmail.com).
 * https://bitbucket.org/VasilSlavchev/simpleloginformwithpm
 * This project is under creative commons license: CC BY-NC-SA.
 * https://creativecommons.org/licenses/by-nc-sa/2.5/bg/
 * https://creativecommons.org/licenses/by-nc-sa/2.5/bg/deed.en
 * -------------------- This is images.js --------------------*/
var bgImages = new Array();
bgImages[0] = 'images/background.jpg';
bgImages[1] = 'images/background1.jpg';
bgImages[2] = 'images/background2.jpg';
bgImages[3] = 'images/background3.jpg';
bgImages[4] = 'images/background4.jpg';
bgImages[5] = 'images/background5.jpg';
bgImages[6] = 'images/background6.jpg';
bgImages[7] = 'images/background7.jpg';
bgImages[8] = 'images/background8.jpg';
isMax = bgImages.length-1;
currImg = 0+Math.round(Math.random()*isMax);
window.onload = function(){
    isImg = bgImages[currImg];
    document.body.style.backgroundImage = "url("+isImg+")"
}