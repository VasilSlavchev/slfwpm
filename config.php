<?php

/*
 * SLFWPM - Simple Login Form With PM Messages.
 * This is config file for Simple PM Message by Vaseto 2014.
 * More at: (VasilSLavchev.info or vasilslavchev@gmail.com).
 * https://bitbucket.org/VasilSlavchev/simpleloginformwithpm
 * This project is under creative commons license: CC BY-NC-SA.
 * https://creativecommons.org/licenses/by-nc-sa/2.5/bg/
 * https://creativecommons.org/licenses/by-nc-sa/2.5/bg/deed.en
*/

    //Session starts here.
    session_start();

    // we include db config file. Please edit database info to match your settings.
    include_once('db_config.php');

    $_SSSION['error_messages'] = '';
    $_SSSION['success_messages'] = '';
    $error_messages = $_SSSION['error_messages'];
    $success_messages = $_SSSION['success_messages'];

    // if (isset($_SESSION['error_messages'])) {
    //     print('<p class="errors center"><i>'.$_SESSION['error_messages'].'</i><p>');
    // }
    // if (isset($_SESSION['success_messages'])) {
    //     print('<p class="success center"><i>'.$_SESSION['success_messages'].'</i><p>');
    // }

    //simple login into database.
    mysql_connect($dbhostname, $dbusername, $dbpassword);
    mysql_select_db($database);

    //here we login into database.
    if (!($db = mysql_connect($dbhostname, $dbusername, $dbpassword))){
        $_SESSION['error_messages'] = "(Error1): Can't connect to this hostname: ".$dbhostname.".";
        exit;
    }
    else {
        if (!(mysql_select_db($database,$db)))  {
            $_SESSION['error_messages'] = "(Error2): Can't connect to this database: ".$database.".";
            exit;
        }
    }
    $_SESSION['error_messages'] = '';
    $_SESSION['success_messages'] = '';

    // $_SESSION['success_messages']  = "(Success1): Connected to database: ".$database.".";
    // $_SESSION['success_messages'] .= "(Success2): Connected to database: ".$database.".";

    // if (isset($_POST['username']) and isset($_POST['password'])){
    //     $username = $_POST['username'];
    //     $password = $_POST['password'];
    //     $query = "SELECT * FROM `users` WHERE username='$username' and password='$password'";

    //     $result = mysql_query($query) or die(mysql_error());
    //     $count = mysql_num_rows($result);

    //     if ($count == 1){
    //         $_SESSION['username'] = $username;
    //         echo '||Session username: "'.$username.' "||.';
    //     }else{

    //         echo "Невалидно Потребителско име или Парола!";
    //         $_SESSION['error_messages'] = "Невалидно Потребителско име или Парола!";
    //     }
    // }

    // if (!empty($username)){
    //     echo '||Table username: "'.$username.' "||.';
    // }
    // else {
    //     echo "wrong username!</center>";
    // }

    //Webmaster email.
    $mail_webmaster = 'mail@dev.slfwpm';

    //Site url domain.
    $site_url = 'http://dev.slfwpm/';

    //Home page file.
    $home_dir = 'index.php';

    //Theme directory.
    $theme = 'classic';

    // break lines
    const BR = PHP_EOL;

?>